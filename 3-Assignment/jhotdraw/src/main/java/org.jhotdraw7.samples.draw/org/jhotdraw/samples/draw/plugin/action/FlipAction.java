/* @(#)BringToFrontAction.java
 * Copyright © The authors and contributors of JHotDraw. MIT License.
 */
package org.jhotdraw.samples.draw.plugin.action;


import org.jhotdraw.draw.action.AbstractSelectedAction;
import org.jhotdraw.draw.Drawing;
import org.jhotdraw.draw.DrawingEditor;
import org.jhotdraw.draw.DrawingView;
import org.jhotdraw.draw.Figure;
import org.jhotdraw.draw.DrawLabels;
import org.jhotdraw.util.ResourceBundleUtil;

import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import javax.swing.JToolBar;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import java.util.Collection;
import java.util.LinkedList;

/**
 * FlipAction.
 *
 * @author Group 007
 * @version $Id$
 */
public class FlipAction extends AbstractSelectedAction {
    private static final long serialVersionUID = 1L;

    public static final String ID = "edit.bringToFront";
    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;

    private int direction;

    /**
     * Creates a new instance.
     */
    public FlipAction(DrawingEditor editor, int direction) {
        super(editor);
        ResourceBundleUtil labels =
                DrawLabels.getLabels();
        labels.configureAction(this, ID);
        updateEnabledState();

        this.direction = direction;
    }

    @Override
    public void actionPerformed(java.awt.event.ActionEvent e) {
        final LinkedList<Figure> figures = new LinkedList<Figure>(getView().getSelectedFigures());
        int direction = this.direction;
        flip(figures, direction);

        fireUndoableEditHappened(new AbstractUndoableEdit() {
            private static final long serialVersionUID = 1L;

            @Override
            public String getPresentationName() {
                ResourceBundleUtil labels =
                        DrawLabels.getLabels();
                return labels.getTextProperty(ID);
            }

            @Override
            public void redo() throws CannotRedoException {
                super.redo();
                FlipAction.flip(figures, direction);
            }

            @Override
            public void undo() throws CannotUndoException {
                super.undo();
                FlipAction.flip(figures, direction);
            }
        });
    }

    public static void flip(Collection<Figure> figures, int direction) {
        for (Figure figure : figures) {
            figure.willChange();

            Rectangle2D.Double bounds = figure.getBounds();

            AffineTransform goToNullpoint = new AffineTransform(new double[]{1.0, 0.0, 0.0, 1.0});
            goToNullpoint.translate(-bounds.x, -bounds.y);
            AffineTransform flipTransform;
            if (direction == FlipAction.HORIZONTAL) {
                flipTransform = new AffineTransform(new double[]{-1.0, 0.0, 0.0, 1.0});
                flipTransform.translate(-bounds.width, 0.0);
            } else {
                flipTransform = new AffineTransform(new double[]{1.0, 0.0, 0.0, -1.0});
                flipTransform.translate(0.0, -bounds.height);
            }
            AffineTransform goBack = new AffineTransform(new double[]{1.0, 0.0, 0.0, 1.0});
            goBack.translate(bounds.x, bounds.y);

            figure.transform(goToNullpoint);
            figure.transform(flipTransform);
            figure.transform(goBack);
            figure.changed();
        }
    }

    public static void addButtons(JToolBar tb, DrawingEditor editor) {
        tb.add(new FlipAction(editor, FlipAction.HORIZONTAL)).setFocusable(false);
        tb.add(new FlipAction(editor, FlipAction.VERTICAL)).setFocusable(false);
    }
}
