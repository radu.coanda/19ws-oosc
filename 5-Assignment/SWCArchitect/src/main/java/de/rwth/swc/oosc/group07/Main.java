package de.rwth.swc.oosc.group07;

import org.jhotdraw.app.Application;
import org.jhotdraw.app.OSXApplication;
import org.jhotdraw.app.SDIApplication;
import org.jhotdraw.util.ResourceBundleUtil;

public class Main {
    public static void main(String[] args) {
        ResourceBundleUtil.setVerbose(true);

        Application app;
        String os = System.getProperty("os.name").toLowerCase();
        if (os.startsWith("mac")) {
            app = new OSXApplication();
        } else if (os.startsWith("win")) {
            app = new SDIApplication();
        } else {
            app = new SDIApplication();
        }

        SWCArchitectModel model = new SWCArchitectModel();
        model.setName("SWCArchitect");
        model.setVersion(Main.class.getPackage().getImplementationVersion());
        model.setCopyright("");
        model.setViewFactory(FloorView::new);

        app.setModel(model);
        app.launch(args);
    }
}
