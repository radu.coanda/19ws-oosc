package de.rwth.swc.oosc.group07.Figures;

import org.jhotdraw.draw.AbstractAttributedFigure;
import org.jhotdraw.draw.AttributeKey;
import org.jhotdraw.draw.AttributeKeys;
import org.jhotdraw.geom.Geom;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class StoveFigure extends AbstractAttributedFigure {
    private static final long serialVersionUID = 1L;

    /**
     * If the attribute IS_QUADRATIC is put to true, all sides of the Stove have
     * the same length.
     */
    public static final AttributeKey<Boolean> IS_QUADRATIC = new AttributeKey<Boolean>("isQuadratic", Boolean.class, false);
    /**
     * The bounds of the Stove figure.
     */
    private Rectangle2D.Double rectangle;

    /** Creates a new instance. */
    public StoveFigure() {
        this(0, 0, 0, 0);
    }

    public StoveFigure(double x, double y, double width, double height) {
        rectangle = new Rectangle2D.Double(x, y, width, height);
    }

    // DRAWING
    @Override
    protected void drawFill(Graphics2D g) {
        Rectangle2D.Double r = (Rectangle2D.Double) rectangle.clone();
        if (get(IS_QUADRATIC)) {
            double side = Math.max(r.width, r.height);
            r.x -= (side - r.width) / 2;
            r.y -= (side - r.height) / 2;
            r.width = r.height = side;
        }

        double grow = AttributeKeys.getPerpendicularFillGrowth(this);
        if (grow != 0d) {
            double w = r.width / 2d;
            double h = r.height / 2d;
            double lineLength = Math.sqrt(w * w + h * h);
            double scale = grow / lineLength;
            double yb = scale * w;
            double xa = scale * h;

            double growx, growy;
            growx = ((yb * yb) / xa + xa);
            growy = ((xa * xa) / yb + yb);

            Geom.grow(r, growx, growy);
        }

        Path2D.Double stove = new Path2D.Double();
        double min = Math.min(r.width, r.height);
        stove.moveTo(r.x, r.y);
        stove.lineTo((r.x + min), (r.y));
        stove.lineTo((r.x + min), (r.y + min));
        stove.lineTo(r.x, (r.y + min));
        stove.lineTo(r.x, r.y);
        stove.lineTo(r.x+min, r.y+min);
        stove.moveTo(r.x + min, r.y);
        stove.lineTo(r.x, r.y+min);
        stove.closePath();
    }

    @Override
    protected void drawStroke(Graphics2D g) {
        Rectangle2D.Double r = (Rectangle2D.Double) rectangle.clone();
        if (get(IS_QUADRATIC)) {
            double side = Math.max(r.width, r.height);
            r.x -= (side - r.width) / 2;
            r.y -= (side - r.height) / 2;
            r.width = r.height = side;
        }

        double grow = AttributeKeys.getPerpendicularDrawGrowth(this);
        if (grow != 0d) {
            double growx, growy;
            double w = r.width / 2d;
            double h = r.height / 2d;
            double lineLength = Math.sqrt(w * w + h * h);
            double scale = grow / lineLength;
            double yb = scale * w;
            double xa = scale * h;

            growx = ((yb * yb) / xa + xa);
            growy = ((xa * xa) / yb + yb);

            Geom.grow(r, growx, growy);
        }

        Path2D.Double stove = new Path2D.Double();
        double min = Math.min(r.width, r.height);
        stove.moveTo(r.x, r.y);
        stove.lineTo((r.x + min), (r.y));
        stove.lineTo((r.x + min), (r.y + min));
        stove.lineTo(r.x, (r.y + min));
        stove.lineTo(r.x, r.y);
        stove.lineTo(r.x+min, r.y+min);
        stove.moveTo(r.x + min, r.y);
        stove.lineTo(r.x, r.y+min);
        stove.closePath();
        g.draw(stove);
    }
// SHAPE AND BOUNDS

    @Override
    public Rectangle2D.Double getBounds() {
        Rectangle2D.Double bounds = (Rectangle2D.Double) rectangle.clone();
        return bounds;
    }

    @Override
    public Rectangle2D.Double getDrawingArea() {
        Rectangle2D.Double r = (Rectangle2D.Double) rectangle.clone();
        if (get(IS_QUADRATIC)) {
            double side = Math.max(r.width, r.height);
            r.x -= (side - r.width) / 2;
            r.y -= (side - r.height) / 2;
            r.width = r.height = side;
        }
        double grow = AttributeKeys.getPerpendicularHitGrowth(this);
        if (grow != 0d) {
            double w = r.width / 2d;
            double h = r.height / 2d;
            double lineLength = Math.sqrt(w * w + h * h);
            double scale = grow / lineLength;
            double yb = scale * w;
            double xa = scale * h;

            double growx, growy;
            growx = ((yb * yb) / xa + xa);
            growy = ((xa * xa) / yb + yb);

            Geom.grow(r, growx, growy);
        }

        return r;
    }

    /**
     * Checks if a Point2D.Double is inside the figure.
     */
    @Override
    public boolean contains(Point2D.Double p) {
        Rectangle2D.Double r = (Rectangle2D.Double) rectangle.clone();
        if (get(IS_QUADRATIC)) {
            double side = Math.max(r.width, r.height);
            r.x -= (side - r.width) / 2;
            r.y -= (side - r.height) / 2;
            r.width = r.height = side;
        }
        //   if (r.contains(p)) {

        double grow = AttributeKeys.getPerpendicularFillGrowth(this);
        if (grow != 0d) {
            double w = r.width / 2d;
            double h = r.height / 2d;
            double lineLength = Math.sqrt(w * w + h * h);
            double scale = grow / lineLength;
            double yb = scale * w;
            double xa = scale * h;

            double growx, growy;
            growx = ((yb * yb) / xa + xa);
            growy = ((xa * xa) / yb + yb);

            Geom.grow(r, growx, growy);
        }

        Path2D.Double stove = new Path2D.Double();
        stove.moveTo((r.x + r.width / 2), r.y);
        stove.lineTo((r.x + r.width), (r.y + r.height / 2));
        stove.lineTo((r.x + r.width / 2), (r.y + r.height));
        stove.lineTo(r.x, (r.y + r.height / 2));
        stove.closePath();
        return stove.contains(p);
    }

    @Override
    public void setBounds(Point2D.Double anchor, Point2D.Double lead) {
        rectangle.x = Math.min(anchor.x, lead.x);
        rectangle.y = Math.min(anchor.y, lead.y);
        rectangle.width = Math.max(0.1, Math.abs(lead.x - anchor.x));
        rectangle.height = Math.max(0.1, Math.abs(lead.y - anchor.y));
    }

    /**
     * Moves the Figure to a new location.
     * @param tx the transformation matrix.
     */
    @Override
    public void transform(AffineTransform tx) {
        Point2D.Double anchor = getStartPoint();
        Point2D.Double lead = getEndPoint();
        setBounds(
                (Point2D.Double) tx.transform(anchor, anchor),
                (Point2D.Double) tx.transform(lead, lead));
    }

    @Override
    public void restoreTransformTo(Object geometry) {
        Rectangle2D.Double r = (Rectangle2D.Double) geometry;
        rectangle.x = r.x;
        rectangle.y = r.y;
        rectangle.width = r.width;
        rectangle.height = r.height;
    }

    @Override
    public Object getTransformRestoreData() {
        return rectangle.clone();
    }

// ATTRIBUTES
// EDITING
// CONNECTING
// COMPOSITE FIGURES
// CLONING

    @Override
    public StoveFigure clone() {
        StoveFigure that = (StoveFigure) super.clone();
        that.rectangle = (Rectangle2D.Double) this.rectangle.clone();
        return that;
    }
// EVENT HANDLING
}
