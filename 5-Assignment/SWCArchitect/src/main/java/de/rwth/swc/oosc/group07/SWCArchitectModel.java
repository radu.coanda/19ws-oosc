package de.rwth.swc.oosc.group07;

import de.rwth.swc.oosc.group07.Figures.ChairFigure;
import de.rwth.swc.oosc.group07.Figures.DoorFigure;
import de.rwth.swc.oosc.group07.Figures.StoveFigure;
import de.rwth.swc.oosc.group07.Figures.TableFigure;
import de.rwth.swc.oosc.group07.Figures.WallFigure;
import de.rwth.swc.oosc.group07.Figures.WindowFigure;
import org.jhotdraw.annotation.Nullable;
import org.jhotdraw.app.Application;
import org.jhotdraw.app.DefaultApplicationModel;
import org.jhotdraw.app.View;
import org.jhotdraw.app.action.edit.*;
import org.jhotdraw.app.action.file.*;
import org.jhotdraw.draw.*;
import org.jhotdraw.draw.action.ButtonFactory;
import org.jhotdraw.draw.tool.*;
import org.jhotdraw.gui.JFileURIChooser;
import org.jhotdraw.gui.URIChooser;
import org.jhotdraw.gui.filechooser.ExtensionFileFilter;
import org.jhotdraw.util.ResourceBundleUtil;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.jhotdraw.draw.AttributeKeys.*;

/**
 * Provides factory methods for creating views, menu bars and toolbars.
 */
public class SWCArchitectModel extends DefaultApplicationModel {

    @Override
    public ActionMap createActionMap(Application a, @Nullable View v) {
        ActionMap m=new ActionMap();
        m.put(NewFileAction.ID, new NewFileAction(a));
        m.put(OpenFileAction.ID, new OpenFileAction(a));
        m.put(SaveFileAction.ID, new SaveFileAction(a,v));
        m.put(SaveFileAsAction.ID, new SaveFileAsAction(a,v));
        m.put(CloseFileAction.ID, new CloseFileAction(a,v));
        m.put(ExportFileAction.ID, new ExportFileAction(a, v));

        m.put(UndoAction.ID, new UndoAction(a,v));
        m.put(RedoAction.ID, new RedoAction(a,v));
        m.put(CutAction.ID, new CutAction());
        m.put(CopyAction.ID, new CopyAction());
        m.put(PasteAction.ID, new PasteAction());
        m.put(DeleteAction.ID, new DeleteAction());
        m.put(DuplicateAction.ID, new DuplicateAction());
        m.put(SelectAllAction.ID, new SelectAllAction());
        m.put(ClearSelectionAction.ID, new ClearSelectionAction());
        return m;
    }


    /**
     * This editor is shared by all views.
     */
    private DefaultDrawingEditor sharedEditor;

    /** Creates a new instance. */
    public SWCArchitectModel() {
    }

    public DefaultDrawingEditor getSharedEditor() {
        if (sharedEditor == null) {
            sharedEditor = new DefaultDrawingEditor();
        }
        return sharedEditor;
    }

    @Override
    public void initView(Application a, View p) {
        if (a.isSharingToolsAmongViews()) {
            ((FloorView) p).setEditor(getSharedEditor());
        }
    }

    /**
     * Creates toolbars for the application.
     * This class always returns an empty list. Subclasses may return other
     * values.
     */
    @Override
    public List<JToolBar> createToolBars(Application a, @Nullable View pr) {
        ResourceBundleUtil labels = DrawLabels.getLabels();
        FloorView p = (FloorView) pr;

        DrawingEditor editor;
        if (p == null) {
            editor = getSharedEditor();
        } else {
            editor = p.getEditor();
        }

        LinkedList<JToolBar> list = new LinkedList<JToolBar>();
        JToolBar tb;

        // SWCArchitect Toolbar
        tb = new JToolBar();
        addSWCArchitectButtonsTo(tb, editor);
        tb.setName(labels.getString("window.swcArchitectToolBar.title"));
        list.add(tb);

        return list;
    }

    private void addSWCArchitectButtonsTo(JToolBar tb, DrawingEditor editor) {
        addDefaultSWCArchitectButtonsTo(tb, editor,
                ButtonFactory.createDrawingActions(editor),
                ButtonFactory.createSelectionActions(editor));
    }

    private void addDefaultSWCArchitectButtonsTo(JToolBar tb, final DrawingEditor editor,
                                                 Collection<Action> drawingActions, Collection<Action> selectionActions) {
        ResourceBundleUtil labels = SWCLabels.getLabels();

        AbstractAttributedFigure af;
        CreationTool ct;
        ConnectionTool cnt;
        ConnectionFigure lc;

        ButtonFactory.addSelectionToolTo(tb, editor, drawingActions, selectionActions);
        tb.addSeparator();

        // Background Image
        ButtonFactory.addToolTo(tb, editor, new ImageTool(new BackgroundImageFigure()), "image.addBackground", labels);

        tb.addSeparator();

        // Elements
        ButtonFactory.addToolTo(tb, editor, ct = new CreationTool(new WallFigure()), "element.addWall", labels);
        af = (AbstractAttributedFigure) ct.getPrototype();
        af.set(FILL_COLOR, Color.BLACK);
        ButtonFactory.addToolTo(tb, editor, new CreationTool(new WindowFigure()), "element.addWindow", labels);
        ButtonFactory.addToolTo(tb, editor, new CreationTool(new DoorFigure()), "element.addDoor", labels);

        tb.addSeparator();

        // Furniture
        ButtonFactory.addToolTo(tb, editor, ct = new CreationTool(new TableFigure()), "furniture.addTable", labels);
        af = (AbstractAttributedFigure) ct.getPrototype();
        af.set(STROKE_WIDTH, 2.0);
        ButtonFactory.addToolTo(tb, editor, new CreationTool(new ChairFigure()), "furniture.addChair", labels);
        ButtonFactory.addToolTo(tb, editor, new CreationTool(new StoveFigure()), "furniture.addStove", labels);

        tb.addSeparator();
        ButtonFactory.addToolTo(tb, editor, new CreationTool(new LineFigure()), "furniture.addStove", labels);
        // Own Furniture
    }

    @Override
    public URIChooser createOpenChooser(Application a, @Nullable View v) {
        JFileURIChooser c = new JFileURIChooser();
        c.addChoosableFileFilter(new ExtensionFileFilter("Drawing .xml","xml"));
        return c;
    }

    @Override
    public URIChooser createSaveChooser(Application a, @Nullable View v) {
        JFileURIChooser c = new JFileURIChooser();
        c.addChoosableFileFilter(new ExtensionFileFilter("Drawing .xml","xml"));
        return c;
    }

    @Override
    public URIChooser createExportChooser(Application a, @Nullable View v) {
        JFileURIChooser c = new JFileURIChooser();
        c.addChoosableFileFilter(new ExtensionFileFilter("Floorplan-Image .png", "png"));
        return c;
    }
}
