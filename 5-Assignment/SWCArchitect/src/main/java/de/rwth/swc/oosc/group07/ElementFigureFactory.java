package de.rwth.swc.oosc.group07;

import org.jhotdraw.draw.*;
import org.jhotdraw.draw.connector.*;
import org.jhotdraw.draw.decoration.ArrowTip;
import org.jhotdraw.draw.liner.CurvedLiner;
import org.jhotdraw.draw.liner.ElbowLiner;
import org.jhotdraw.xml.DefaultDOMFactory;

public class ElementFigureFactory extends DefaultDOMFactory {
    private static final Object[][] classTagArray = {
            { DefaultDrawing.class, "drawing" },
            { QuadTreeDrawing.class, "drawing" },
            { DiamondFigure.class, "diamond" },
            { TriangleFigure.class, "triangle" },
            { BezierFigure.class, "bezier" },
            { RectangleFigure.class, "r" },
            { RoundRectangleFigure.class, "rr" },
            { LineFigure.class, "l" },
            { BezierFigure.class, "b" },
            { LineConnectionFigure.class, "lnk" },
            { EllipseFigure.class, "e" },
            { TextFigure.class, "t" },
            { TextAreaFigure.class, "ta" },
            { ImageFigure.class, "image" },
            { GroupFigure.class, "g" },

            { ArrowTip.class, "arrowTip" },
            { ChopRectangleConnector.class, "rConnector" },
            { ChopEllipseConnector.class, "ellipseConnector" },
            { ChopRoundRectangleConnector.class, "rrConnector" },
            { ChopTriangleConnector.class, "triangleConnector" },
            { ChopDiamondConnector.class, "diamondConnector" },
            { ChopBezierConnector.class, "bezierConnector" },

            { ElbowLiner.class, "elbowLiner" },
            { CurvedLiner.class, "curvedLiner" },
    };
    private static final Object[][] enumTagArray = {
            { AttributeKeys.StrokePlacement.class, "strokePlacement" },
            { AttributeKeys.StrokeType.class, "strokeType" },
            { AttributeKeys.Underfill.class, "underfill" },
            { AttributeKeys.Orientation.class, "orientation" },
    };

    /** Creates a new instance. */
    public ElementFigureFactory() {
        for (Object[] o : classTagArray) {
            addStorableClass((String) o[1], (Class) o[0]);
        }
        for (Object[] o : enumTagArray) {
            addEnumClass((String) o[1], (Class) o[0]);
        }
    }
}
