package de.rwth.swc.group07;

public class StandardOutputLogger extends Logger{
    @Override
    public void log(String message) {
        System.out.println(message);
    }
}
