package de.rwth.swc.group07;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class Logger {
    public void log(String message) {
        try {
            Path filepath = Paths.get("./log.log");
            if (!Files.exists(filepath)) {
                Files.createFile(filepath);
            }
            Files.write(filepath, message.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
