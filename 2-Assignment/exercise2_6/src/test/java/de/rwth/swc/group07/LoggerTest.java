package de.rwth.swc.group07;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Unit test for simple App.
 */
public class LoggerTest {
    @Test
    public void shouldLogToFile() throws IOException {

        Path filepath = Paths.get("./log.log");
        String message = "this is in a file, yes?";

        //create or delete file
        if (Files.exists(filepath)) {
            Files.delete(filepath);
        }

        Logger logger = new Logger();
        logger.log(message);

        //check if file exists and check content
        assertTrue(Files.exists(filepath));
        assertEquals(message, new String(Files.readAllBytes(filepath)));
    }

    @Test
    public void shouldLogToStandardOutput(){
        //set standard output for testing
        final ByteArrayOutputStream myOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(myOut));

        String message = "this is in a file, yes?";

        Logger logger = new StandardOutputLogger();
        logger.log(message);

        //get standardOutput
        final String standardOutput = myOut.toString();

        assertEquals(message+System.lineSeparator() , standardOutput);
    }
}
