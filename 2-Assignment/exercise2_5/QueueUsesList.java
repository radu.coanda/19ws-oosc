import java.util.ArrayList;

public class QueueUsesList<E> {
    ArrayList<E> elements = new ArrayList<>();
    public void enqueue(E element){
        elements.add(element);
    }

    public E dequeue(){
        if(this.isEmpty()) {
            return null;
        }
        E element = elements.get(0);
        elements.remove(0);
        return element;
    }

    public E getFront(){
        if(this.isEmpty()){
            return null;
        }
        return elements.get(0);
    }

    public void clear(){
        elements.clear();
    }

    public boolean isEmpty(){
        return elements.isEmpty();
    }

    public int size(){
        return elements.size();
    }
}
