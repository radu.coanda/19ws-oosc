import java.util.ArrayList;

public class Queue<E> extends ArrayList<E> {
    public void enqueue(E element){
        this.add(element);
    }

    public E dequeue(){
        if(this.isEmpty()) {
            return null;
        }
        E element = this.get(0);
        this.remove(0);
        return element;
    }

    public E getFront(){
        if(this.isEmpty()){
            return null;
        }
        return this.get(0);
    }
}
