import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class QueueTest {
    Queue<Integer> integers;

    @Before
    public void setUp() throws Exception {
        integers = new Queue<>();
    }

    @Test
    public void enqueue() {
        integers.enqueue(1);
        integers.enqueue(2);
        integers.enqueue(3);

        Assert.assertEquals(3, integers.size());
    }

    @Test
    public void dequeue() {
        integers.enqueue(1);
        integers.enqueue(2);
        integers.enqueue(3);

        Assert.assertEquals(3, integers.size());
        Assert.assertEquals(1, integers.dequeue().intValue());
        Assert.assertEquals(2, integers.size());
    }

    @Test
    public void dequeueEmpty() {
        Assert.assertEquals(null, integers.dequeue());
    }

    @Test
    public void isEmpty() {
        Assert.assertEquals(true, integers.isEmpty());
        integers.enqueue(1);
        Assert.assertEquals(false, integers.isEmpty());
    }

    @Test
    public void getFront() {
        integers.enqueue(1);
        integers.enqueue(2);
        integers.enqueue(3);

        Assert.assertEquals(3, integers.size());
        Assert.assertEquals(1, integers.getFront().intValue());
        Assert.assertEquals(3, integers.size());
    }

    @Test
    public void getFrontEmpty() {
        Assert.assertEquals(null, integers.getFront());
    }

    @Test
    public void clear() {
        integers.enqueue(1);
        integers.enqueue(2);
        integers.enqueue(3);

        Assert.assertEquals(3, integers.size());

        integers.clear();

        Assert.assertEquals(0, integers.size());
    }
}
