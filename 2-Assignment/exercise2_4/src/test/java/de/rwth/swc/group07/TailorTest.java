package de.rwth.swc.group07;

import org.junit.Assert;
import org.junit.Test;

public class TailorTest {

    @Test
    public void testDoJob() {
        JobPerson j = new Tailor();
        String result = j.doJob();
        Assert.assertEquals("tailoring...", result);
    }
}
