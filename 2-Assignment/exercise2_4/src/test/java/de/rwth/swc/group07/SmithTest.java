package de.rwth.swc.group07;

import org.junit.Assert;
import org.junit.Test;

public class SmithTest {

    @Test
    public void testDoJob() {
        JobPerson j = new Smith();
        String result = j.doJob();
        Assert.assertEquals("smithing...", result);
    }
}
