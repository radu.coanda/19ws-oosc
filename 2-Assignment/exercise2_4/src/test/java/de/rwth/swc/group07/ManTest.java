package de.rwth.swc.group07;

import org.junit.Test;
import org.junit.Assert;

public class ManTest {

    @Test
    public void testLikes() {
        GenderPerson g = new Man();
        String result = g.likes();
        Assert.assertEquals("woman", result);
    }
}
