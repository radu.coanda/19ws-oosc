package de.rwth.swc.group07;

import org.junit.Assert;
import org.junit.Test;

public class WomanTest {

    @Test
    public void testLikes() {
        GenderPerson g = new Woman();
        String result = g.likes();
        Assert.assertEquals("man", result);
    }
}
