package de.rwth.swc.group07;

public abstract class GenderPerson extends Person {
    public abstract String likes();
}
