package de.rwth.swc.group07;

public class Woman extends GenderPerson {
    @Override
    public String likes() {
        return "man";
    }
}
