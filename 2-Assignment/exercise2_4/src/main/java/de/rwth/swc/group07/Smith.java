package de.rwth.swc.group07;

public class Smith extends JobPerson{
    @Override
    public String doJob() {
        return "smithing...";
    }
}
