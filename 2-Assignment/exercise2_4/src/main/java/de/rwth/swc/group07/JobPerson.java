package de.rwth.swc.group07;

public abstract class JobPerson extends Person {
    public abstract String doJob();
}
