package de.rwth.swc.group07;

public class Tailor extends JobPerson{
    @Override
    public String doJob() {
        return "tailoring...";
    }
}
