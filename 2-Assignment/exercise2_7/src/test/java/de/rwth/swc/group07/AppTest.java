package de.rwth.swc.group07;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    @Test
    public void should_computeBinaryTree_when_simplePlusExpressions() {
        BinaryNode computation = new Plus(new Num(3), new Num(2));
        int target = 5;
        assertTrue(computation.process() == target);
    }

    @Test
    public void should_computeBinaryTree_when_simpleMultExpressions() {
        BinaryNode computation = new Mult(new Num(3), new Num(2));
        int target = 6;
        assertTrue(computation.process() == target);
    }

    @Test
    public void should_computeBinaryTree_when_simpleMinusExpressions() {
        BinaryNode computation = new Minus(new Num(3), new Num(2));
        int target = 1;
        assertTrue(computation.process() == target);
    }

    @Test
    public void should_computeBinaryTree_when_simpleDivisionExpressions() {
        BinaryNode computation = new Division(new Num(3), new Num(2));
        float target = 1.5f;
        assertTrue(computation.process() == target);
    }

    @Test
    public void should_computeBinaryTree_when_simpleModuloExpressions() {
        BinaryNode computation = new Modulo(new Num(3), new Num(2.3f));
        float target = 3f % 2.3f;
        assertTrue(computation.process() == target);
    }

    @Test
    public void should_computeBinaryTree_when_complexExpressions1() {
        BinaryNode computation = new Division(new Plus(new Num(3), new Num(2)),
                new Division(new Num(3), new Mult(new Num(3), new Num(2))));
        float target = 10;
        assertTrue(computation.process() == target);
    }
}
