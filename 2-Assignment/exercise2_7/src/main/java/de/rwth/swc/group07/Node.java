package de.rwth.swc.group07;

public abstract class Node 
{
    public abstract float process();
}
