package de.rwth.swc.group07;

/**
 * Minus
 */
public class Minus extends BinaryNode{

    public Minus(Node left, Node right) {
        super(left, right);
    }

    @Override
    public float process() {
        return this.getLeft().process() - this.getRight().process();
    }
}