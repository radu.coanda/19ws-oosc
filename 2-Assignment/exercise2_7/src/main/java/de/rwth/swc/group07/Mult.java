package de.rwth.swc.group07;

/**
 * Mult
 */
public class Mult extends BinaryNode{

    public Mult(Node left, Node right) {
        super(left, right);
    }

    @Override
    public float process() {
        return this.getLeft().process() * this.getRight().process();
    }
}