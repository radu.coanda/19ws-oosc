package de.rwth.swc.group07;

/**
 * Modulo
 */
public class Modulo extends BinaryNode {

    public Modulo(Node left, Node right) {
        super(left, right);
    }

    @Override
    public float process() {
        return this.getLeft().process() % this.getRight().process();
    }

    
}