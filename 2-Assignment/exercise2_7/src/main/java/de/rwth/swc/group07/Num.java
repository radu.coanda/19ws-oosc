package de.rwth.swc.group07;

/**
 * Number
 */
public class Num extends Node {

    private float value;

    public Num(float value) {
        this.value = value;
    }

    @Override
    public float process() {
        return this.getValue();
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}