package de.rwth.swc.group07;

/**
 * Division
 */
public class Division extends BinaryNode {

    public Division(Node left, Node right) {
        super(left, right);
    }

    @Override
    public float process() {
        return this.getLeft().process() / this.getRight().process();
    }

}