package de.rwth.swc.group07;

public class Plus extends BinaryNode 
{
    public Plus(Node left, Node right) {
        super(left, right);
    }

    @Override
    public float process() {
        return this.getLeft().process() + this.getRight().process();
    }
}
