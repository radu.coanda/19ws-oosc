package de.rwth.swc.group07;

public abstract class BinaryNode extends Node {
    private Node left;
    private Node right; 

    public abstract float process();

    public BinaryNode(Node left, Node right) {
        this.left = left;
        this.right = right;
    }

    public Node getLeft() {
        return this.left;
    }

    public Node getRight() {
        return this.right;
    }
    
}
