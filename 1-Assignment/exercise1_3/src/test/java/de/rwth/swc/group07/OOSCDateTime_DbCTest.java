package de.rwth.swc.group07;

import org.junit.Test;
import org.valid4j.errors.RequireViolation;

public class OOSCDateTime_DbCTest {
    private final int YEAR = 2019;
    private final int MONTH = 10;
    private final int DAY = 19;
    private final int HOUR = 12;
    private final int MINUTE = 30;
    private final int SECOND = 30;

    @Test(expected = RequireViolation.class)
    public void setTime1() {
        OOSCDateTime date = new OOSCDateTime(0, 1, 1, 0, 0, 0);
        date.setTime(-12, MINUTE, SECOND);
    }

    @Test(expected = RequireViolation.class)
    public void setTime2() {
        OOSCDateTime date = new OOSCDateTime(0, 1, 1, 0, 0, 0);
        date.setTime(YEAR, 60, SECOND);
    }

    @Test(expected = RequireViolation.class)
    public void setTime3() {
        OOSCDateTime date = new OOSCDateTime(0, 1, 1, 0, 0, 0);
        date.setTime(YEAR, MINUTE, 60);
    }

    @Test(expected = RequireViolation.class)
    public void setHour() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.setHour(24);
    }

    @Test(expected = RequireViolation.class)
    public void setMinute() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.setMinute(-1);
    }

    @Test(expected = RequireViolation.class)
    public void setSecond() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.setSecond(60);
    }

    @Test(expected = RequireViolation.class)
    public void addHours() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.addHours(-35);
    }

    @Test(expected = RequireViolation.class)
    public void addMinutes() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.addMinutes(-3);
    }

    @Test(expected = RequireViolation.class)
    public void addSeconds() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.addSeconds(-5);
    }

    @Test(expected = RequireViolation.class)
    public void removeHours() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.removeHours(-5);
    }

    @Test(expected = RequireViolation.class)
    public void removeMinutes() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.removeMinutes(-15);
    }

    @Test(expected = RequireViolation.class)
    public void removeSeconds() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.removeSeconds(-25);
    }


    @Test(expected = RequireViolation.class)
    public void timeBetween0() {
        OOSCDateTime date1 = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        OOSCDateTime date2 = null;
        date1.timeBetween(0, date2);
    }

    @Test(expected = RequireViolation.class)
    public void timeBetween1() {
        OOSCDateTime date1 = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        OOSCDateTime date2 = null;
        date1.timeBetween(1, date2);
    }

    @Test(expected = RequireViolation.class)
    public void timeBetween2() {
        OOSCDateTime date1 = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        OOSCDateTime date2 = null;
        date1.timeBetween(2, date2);
    }

    @Test(expected = RequireViolation.class)
    public void timeBetween3() {
        OOSCDateTime date1 = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        OOSCDateTime date2 = null;
        date1.timeBetween(3, date2);
    }

    @Test(expected = RequireViolation.class)
    public void timeBetween4() {
        OOSCDateTime date1 = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        OOSCDateTime date2 = null;
        date1.timeBetween(4, date2);
    }

    @Test(expected = RequireViolation.class)
    public void timeBetween5() {
        OOSCDateTime date1 = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        OOSCDateTime date2 = null;
        date1.timeBetween(5, date2);
    }
}
