package de.rwth.swc.group07;

import org.junit.Assert;
import org.junit.Test;

import java.time.*;

public class OOSCDateTimeTest {

    private final int YEAR = 2019;
    private final int MONTH = 10;
    private final int DAY = 19;
    private final int HOUR = 12;
    private final int MINUTE = 30;
    private final int SECOND = 30;

    @Test
    public void setTime(){
        OOSCDateTime date = new OOSCDateTime(0, 1, 1, 0, 0, 0);
        date.setTime(HOUR, MINUTE, SECOND);

        Assert.assertEquals(HOUR, date.getHour());
        Assert.assertEquals(MINUTE, date.getMinute());
        Assert.assertEquals(SECOND, date.getSecond());
    }

    @Test
    public void setHour() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.setHour(23);

        Assert.assertEquals(23, date.getHour());
    }

    @Test
    public void setMinute() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.setMinute(23);

        Assert.assertEquals(23, date.getMinute());
    }

    @Test
    public void setSecond() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.setSecond(23);

        Assert.assertEquals(23, date.getSecond());
    }

    @Test
    public void getHour() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        Assert.assertEquals(HOUR, date.getHour());
    }

    @Test
    public void getMinute() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        Assert.assertEquals(MINUTE, date.getMinute());
    }

    @Test
    public void getSecond() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        Assert.assertEquals(SECOND, date.getSecond());
    }

    @Test
    public void addHours() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.addHours(35);
        Assert.assertEquals(YEAR, date.getYear());
        Assert.assertEquals(MONTH, date.getMonth());
        Assert.assertEquals(DAY+1, date.getDay());
        Assert.assertEquals(23, date.getHour());
        Assert.assertEquals(MINUTE, date.getMinute());
        Assert.assertEquals(SECOND, date.getSecond());
    }

    @Test
    public void addMinutes() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.addMinutes(135);
        Assert.assertEquals(YEAR, date.getYear());
        Assert.assertEquals(MONTH, date.getMonth());
        Assert.assertEquals(DAY, date.getDay());
        Assert.assertEquals(HOUR+2, date.getHour());
        Assert.assertEquals(45, date.getMinute());
        Assert.assertEquals(SECOND, date.getSecond());
    }

    @Test
    public void addSeconds() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.addSeconds(135);
        Assert.assertEquals(YEAR, date.getYear());
        Assert.assertEquals(MONTH, date.getMonth());
        Assert.assertEquals(DAY, date.getDay());
        Assert.assertEquals(HOUR, date.getHour());
        Assert.assertEquals(MINUTE+2, date.getMinute());
        Assert.assertEquals(45, date.getSecond());
    }

    @Test
    public void removeHours() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.removeHours(19);
        Assert.assertEquals(YEAR, date.getYear());
        Assert.assertEquals(MONTH, date.getMonth());
        Assert.assertEquals(DAY-1, date.getDay());
        Assert.assertEquals(17, date.getHour());
        Assert.assertEquals(MINUTE, date.getMinute());
        Assert.assertEquals(SECOND, date.getSecond());
    }

    @Test
    public void removeMinutes() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.removeMinutes(190);
        Assert.assertEquals(YEAR, date.getYear());
        Assert.assertEquals(MONTH, date.getMonth());
        Assert.assertEquals(DAY, date.getDay());
        Assert.assertEquals(HOUR-3, date.getHour());
        Assert.assertEquals(20, date.getMinute());
        Assert.assertEquals(SECOND, date.getSecond());
    }

    @Test
    public void removeSeconds() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        date.removeSeconds(190);
        Assert.assertEquals(YEAR, date.getYear());
        Assert.assertEquals(MONTH, date.getMonth());
        Assert.assertEquals(DAY, date.getDay());
        Assert.assertEquals(HOUR, date.getHour());
        Assert.assertEquals(MINUTE-3, date.getMinute());
        Assert.assertEquals(20, date.getSecond());
    }

    @Test
    public void timeBetween() {
        OOSCDateTime date1 = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        OOSCDateTime date2 = new OOSCDateTime(YEAR+1, MONTH, DAY, HOUR, MINUTE, SECOND);
        Assert.assertEquals(1, date1.timeBetween(0, date2));
        Assert.assertEquals(12, date1.timeBetween(1, date2));
        Assert.assertEquals(366, date1.timeBetween(2, date2));
        Assert.assertEquals(8784, date1.timeBetween(OOSCDateTime.DATETYPE_HOUR, date2));
        Assert.assertEquals(527040, date1.timeBetween(OOSCDateTime.DATETYPE_MINUTE, date2));
        Assert.assertEquals(31622400, date1.timeBetween(OOSCDateTime.DATETYPE_SECOND, date2));
    }

    @Test
    public void syncWithUTCTimeServer() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);

        date.syncWithUTCTimeserver();
        ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);

        Assert.assertEquals(utc.getYear(), date.getYear());
        Assert.assertEquals(utc.getMonthValue(), date.getMonth());
        Assert.assertEquals(utc.getDayOfMonth(), date.getDay());
        Assert.assertEquals(utc.getHour(), date.getHour());
        Assert.assertEquals(utc.getMinute(), date.getMinute());
        Assert.assertTrue(utc.getSecond() >= date.getSecond() && utc.getSecond() <= date.getSecond()+1);
    }

    @Test
    public void toStringTest() {
        OOSCDateTime date = new OOSCDateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);
        Assert.assertEquals("19.10.2019 12:30:30", date.toString());
    }
}
