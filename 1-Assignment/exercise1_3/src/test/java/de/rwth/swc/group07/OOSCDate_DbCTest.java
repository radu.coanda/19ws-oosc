package de.rwth.swc.group07;

import org.junit.Test;
import org.valid4j.errors.RequireViolation;

public class OOSCDate_DbCTest {
    private final int YEAR = 2019;
    private final int MONTH = 10;
    private final int DAY = 19;

    @Test(expected = RequireViolation.class)
    public void setDate(){
        OOSCDate date = new OOSCDate(0, 1, 1);
        date.setDate(-12, MONTH, DAY);
        date.setDate(YEAR, 13, DAY);
        date.setDate(YEAR, MONTH, 0);
    }

    @Test(expected = RequireViolation.class)
    public void setYear() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.setYear(-2);
    }

    @Test(expected = RequireViolation.class)
    public void setMonth() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.setMonth(0);
    }

    @Test(expected = RequireViolation.class)
    public void setDay() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.setDay(32);
    }

    @Test(expected = RequireViolation.class)
    public void addDays() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.addDays(-35);
    }

    @Test(expected = RequireViolation.class)
    public void addMonths() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.addMonths(-3);
    }

    @Test(expected = RequireViolation.class)
    public void addYears() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.addYears(-5);
    }

    @Test(expected = RequireViolation.class)
    public void removeDays() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.removeDays(-5);
        date.removeDays(1);
    }

    @Test(expected = RequireViolation.class)
    public void removeMonths() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.removeMonths(-15);
        date.removeMonths(1);
    }

    @Test(expected = RequireViolation.class)
    public void removeYears() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.removeYears(-25);
        date.removeYears(1);
    }

    @Test(expected = RequireViolation.class)
    public void daysBetween() {
        OOSCDate date1 = new OOSCDate(YEAR, MONTH, DAY);
        OOSCDate date2 = null;
        date1.daysBetween(date2);
    }

    @Test(expected = RequireViolation.class)
    public void timeBetween() {
        OOSCDate date1 = new OOSCDate(YEAR, MONTH, DAY);
        OOSCDate date2 = null;
        date1.timeBetween(0, date2);
        date1.timeBetween(1, date2);
        date1.timeBetween(2, date2);
    }
}
