package de.rwth.swc.group07;

import static java.time.temporal.ChronoUnit.*;
import static org.valid4j.Assertive.ensure;
import static org.valid4j.Assertive.require;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class OOSCDateTime extends OOSCDate {

    public static final int DATETYPE_HOUR = 3;
    public static final int DATETYPE_MINUTE = 4;
    public static final int DATETYPE_SECOND = 5;

    private int hour;
    private int minute;
    private int second;

    public OOSCDateTime(int year, int month, int day, int hour, int minute, int second) {
        super(year, month, day);
        setTime(hour, minute, second);
    }

    public void setTime(int hour, int minute, int second) {
        require(hour >= 0 && hour <= 23, "pre-v: hour must be between 0 and 23 (inclusive)");
        require(minute >= 0 && minute <= 59, "pre-v: minute must be between 0 and 59 (inclusive)");
        require(second >= 0 && second <= 59, "pre-v: second must be between 0 and 59 (inclusive)");

        this.hour = hour;
        this.minute = minute;
        this.second = second;

        ensure(this.hour == hour, "post-v: hour is incorrect");
        ensure(this.minute == minute, "post-v: minute is incorrect");
        ensure(this.second == second, "post-v: second is incorrect");
        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    public void setHour(int hour) {
        require(hour >= 0 && hour <= 23, "pre-v: hour must be between 0 and 23 (inclusive)");

        this.hour = hour;

        ensure(this.hour == hour, "post-v: hour is incorrect");
        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    public void setMinute(int minute) {
        require(minute >= 0 && minute <= 59, "pre-v: minute must be between 0 and 59 (inclusive)");

        this.minute = minute;

        ensure(this.minute == minute, "post-v: minute is incorrect");
        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    public void setSecond(int second) {
        require(second >= 0 && second <= 59, "pre-v: second must be between 0 and 59 (inclusive)");

        this.second = second;

        ensure(this.second == second, "post-v: second is incorrect");
        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    public int getHour() {
        require(this.dateIsValid(), "post-v: date is not valid");
        return this.hour;
    }

    public int getMinute() {
        require(this.dateIsValid(), "post-v: date is not valid");
        return this.minute;
    }

    public int getSecond() {
        require(this.dateIsValid(), "post-v: date is not valid");
        return this.second;
    }

    public void addHours(int hoursToAdd) {
        require(hoursToAdd >= 0, "pre-v: hoursToAdd must be greater or equal to zero");

        LocalDateTime dateTime = toLocalDatetime().plusHours(hoursToAdd);
        setFromLocalDateTime(dateTime);
    }

    public void addMinutes(int minutesToAdd) {
        require(minutesToAdd >= 0, "pre-v: minutesToAdd must be greater or equal to zero");

        LocalDateTime dateTime = toLocalDatetime().plusMinutes(minutesToAdd);
        setFromLocalDateTime(dateTime);
    }

    public void addSeconds(int secondsToAdd) {
        require(secondsToAdd >= 0, "pre-v: secondsToAdd must be greater or equal to zero");

        LocalDateTime dateTime = toLocalDatetime().plusSeconds(secondsToAdd);
        setFromLocalDateTime(dateTime);
    }

    public void removeHours(int hoursToRemove) {
        require(hoursToRemove >= 0, "pre-v: hoursToRemove must be greater or equal to zero");
        OOSCDateTime dateZero = new OOSCDateTime(0, 1, 1, 0, 0, 0);
        require(dateZero.timeBetween(OOSCDateTime.DATETYPE_HOUR, this) >= hoursToRemove, "pre-v: hoursToRemove must be less than total number of hours of current date");

        LocalDateTime dateTime = toLocalDatetime().minusHours(hoursToRemove);
        setFromLocalDateTime(dateTime);
    }

    public void removeMinutes(int minutesToRemove) {
        require(minutesToRemove >= 0, "pre-v: minutesToRemove must be greater or equal to zero");
        OOSCDateTime dateZero = new OOSCDateTime(0, 1, 1, 0, 0, 0);
        require(dateZero.timeBetween(OOSCDateTime.DATETYPE_MINUTE, this) >= minutesToRemove, "pre-v: minutesToRemove must be less than total number of minutes of current date");

        LocalDateTime dateTime = toLocalDatetime().minusMinutes(minutesToRemove);
        setFromLocalDateTime(dateTime);
    }

    public void removeSeconds(int secondsToRemove) {
        require(secondsToRemove >= 0, "pre-v: secondsToRemove must be greater or equal to zero");
        OOSCDateTime dateZero = new OOSCDateTime(0, 1, 1, 0, 0, 0);
        require(dateZero.secondsBetween(this) >= secondsToRemove, "pre-v: secondsToRemove must be less than total number of minutes of current date");

        LocalDateTime dateTime = toLocalDatetime().minusSeconds(secondsToRemove);
        setFromLocalDateTime(dateTime);
    }

    public int timeBetween(int type, OOSCDateTime otherDate) {
        require(otherDate != null, "pre-v: otherDate must not be null");
        require(type >= 0 && type <= 5, "pre-v: type must be between 0 and 5");
        require(this.dateIsValid(), "pre-v: date is not valid");

        if (type <= 2) {
            return super.timeBetween(type, otherDate);
        }

        LocalDateTime startDate = toLocalDatetime();
        LocalDateTime endDate = LocalDateTime.of(otherDate.getYear(), otherDate.getMonth(), otherDate.getDay(), otherDate.getHour(), otherDate.getMinute(), otherDate.getSecond());
        switch (type) {
            case DATETYPE_HOUR:
                return (int) startDate.until(endDate, HOURS);
            case DATETYPE_MINUTE:
                return (int) startDate.until(endDate, MINUTES);
            case DATETYPE_SECOND:
                return (int) startDate.until(endDate, SECONDS);
            default:
                return 0;
        }
    }

    private long secondsBetween(OOSCDateTime otherDate){
        LocalDateTime startDate = toLocalDatetime();
        LocalDateTime endDate = LocalDateTime.of(otherDate.getYear(), otherDate.getMonth(), otherDate.getDay(), otherDate.getHour(), otherDate.getMinute(), otherDate.getSecond());
        return startDate.until(endDate, SECONDS);
    }

    @Override
    public void syncWithUTCTimeserver() {
        try {
            URL url = new URL("https://andthetimeis.com/utc/now");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(5000);

            connection.setRequestMethod("GET");
            connection.setReadTimeout(5000);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer fetchedDate = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                fetchedDate.append(inputLine);
            }
            in.close();

            connection.disconnect();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss +00:00");
            LocalDateTime date = LocalDateTime.parse(fetchedDate, formatter);

            this.setDate(date.getYear(), date.getMonthValue(), date.getDayOfMonth());
            this.setTime(date.getHour(), date.getMinute(), date.getSecond());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    @Override
    public String toString() {
        return super.toString()+ " "+this.getHour()+":"+this.getMinute()+":"+this.getSecond();
    }

    private LocalDateTime toLocalDatetime(){
        return LocalDateTime.of(this.getYear(), this.getMonth(), this.getDay(), this.hour, this.minute, this.second);
    }

    private void setFromLocalDateTime(LocalDateTime dateTime) {
        this.setYear(dateTime.getYear());
        this.setMonth(dateTime.getMonthValue());
        this.setDay(dateTime.getDayOfMonth());
        this.hour = dateTime.getHour();
        this.minute = dateTime.getMinute();
        this.second = dateTime.getSecond();

        ensure(this.dateIsValid(), "post-v: date is not valid");
    }
}
