package de.rwth.swc.group07;

import static org.valid4j.Assertive.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;

import static java.time.temporal.ChronoUnit.*;

public class OOSCDate implements DateInterface {

    private int year;
    private int month;
    private int day;

    public OOSCDate(int year, int month, int day) {
        this.setDate(year, month, day);
    }

    @Override
    public void setDate(int year, int month, int day) {
        require(year >= 0, "pre-v: year must be greater or equal to zero");
        require(month >= 1 && month <= 12, "pre-v: month must be between 1 and 12");
        require(dayIsValid(year, month, day), "pre-v: day must be a valid day in the given month");

        this.year = year;
        this.month = month;
        this.day = day;

        ensure(this.year == year, "post-v: year is incorrect");
        ensure(this.month == month, "post-v: month is incorrect");
        ensure(this.day == day, "post-v: day is incorrect");
        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    @Override
    public void setYear(int year) {
        require(year >= 0, "pre-v: year must be greater or equal to zero");

        this.year = year;

        ensure(this.year == year, "post-v: year is incorrect");
        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    @Override
    public void setMonth(int month) {
        require(month >= 1 && month <= 12, "pre-v: month must be between 1 and 12");

        this.month = month;

        ensure(this.month == month, "post-v: month is incorrect");
        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    @Override
    public void setDay(int day) {
        require(dayIsValid(this.getYear(), this.getMonth(), day), "pre-v: day must be a valid day in the given month");

        this.day = day;

        ensure(this.day == day, "post-v: day is incorrect");
        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    @Override
    public int getYear() {
        require(this.dateIsValid(), "post-v: date is not valid");
        return this.year;
    }

    @Override
    public int getMonth() {
        require(this.dateIsValid(), "post-v: date is not valid");
        return this.month;
    }

    @Override
    public int getDay() {
        require(this.dateIsValid(), "post-v: date is not valid");
        return this.day;
    }

    @Override
    public void addDays(int daysToAdd) {
        require(daysToAdd >= 0, "pre-v: daysToAdd must be greater or equal to zero");

        LocalDate date = LocalDate.of(this.year, this.month, this.day).plusDays(daysToAdd);
        this.year = date.getYear();
        this.month = date.getMonthValue();
        this.day = date.getDayOfMonth();

        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    @Override
    public void addMonths(int monthsToAdd) {
        require(monthsToAdd >= 0, "pre-v: monthsToAdd must be greater or equal to zero");

        LocalDate date = LocalDate.of(this.year, this.month, this.day).plusMonths(monthsToAdd);
        this.year = date.getYear();
        this.month = date.getMonthValue();

        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    @Override
    public void addYears(int yearsToAdd) {
        require(yearsToAdd >= 0, "pre-v: yearsToAdd must be greater or equal to zero");

        this.year += yearsToAdd;

        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    @Override
    public void removeDays(int daysToRemove) {
        require(daysToRemove >= 0, "pre-v: daysToRemove must be greater or equal to zero");
        OOSCDate dateZero = new OOSCDate(0, 1, 1);
        require(dateZero.daysBetween(this) >= daysToRemove, "pre-v: daysToRemove must be less than total number of months of current date");

        LocalDate date = LocalDate.of(this.year, this.month, this.day).minusDays(daysToRemove);
        this.year = date.getYear();
        this.month = date.getMonthValue();
        this.day = date.getDayOfMonth();

        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    @Override
    public void removeMonths(int monthsToRemove) {
        require(monthsToRemove >= 0, "pre-v: monthsToRemove must be greater or equal to zero");
        require((this.year * 12 + this.month) > monthsToRemove, "pre-v: monthsToRemove must be less than total number of months of current date");

        LocalDate date = LocalDate.of(this.year, this.month, this.day).minusMonths(monthsToRemove);
        this.year = date.getYear();
        this.month = date.getMonthValue();

        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    @Override
    public void removeYears(int yearsToRemove) {
        require(yearsToRemove >= 0, "pre-v: yearsToRemove must be greater or equal to zero");
        require(this.year >= yearsToRemove, "pre-v: yearsToRemove must be less or equal to current year");

        this.year -= yearsToRemove;

        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    @Override
    public int daysBetween(DateInterface otherDate) {
        require(otherDate != null, "pre-v: otherDate must not be null");
        require(this.dateIsValid(), "post-v: date is not valid");
        return timeBetween(DATETYPE_DAY, otherDate);
    }

    @Override
    public int timeBetween(int type, DateInterface otherDate) {
        require(otherDate != null, "pre-v: otherDate must not be null");
        require(type >= 0 && type <= 2, "pre-v: type must be between 0 and 2");
        require(this.dateIsValid(), "post-v: date is not valid");

        LocalDate startDate = LocalDate.of(this.year, this.month, this.day);
        LocalDate endDate = LocalDate.of(otherDate.getYear(), otherDate.getMonth(), otherDate.getDay());
        switch (type) {
            case DATETYPE_YEAR:
                return (int) startDate.until(endDate, YEARS);
            case DATETYPE_MONTH:
                return (int) startDate.until(endDate, MONTHS);
            case DATETYPE_DAY:
                return (int) startDate.until(endDate, DAYS);
            default:
                return 0;
        }
    }

    @Override
    public void syncWithUTCTimeserver() {
        try {
            URL url = new URL("https://andthetimeis.com/utc/now");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(5000);

            connection.setRequestMethod("GET");
            connection.setReadTimeout(5000);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer fetchedDate = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                fetchedDate.append(inputLine);
            }
            in.close();

            connection.disconnect();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss +00:00");
            LocalDate date = LocalDate.parse(fetchedDate, formatter);

            this.year = date.getYear();
            this.month = date.getMonthValue();
            this.day = date.getDayOfMonth();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        ensure(this.dateIsValid(), "post-v: date is not valid");
    }

    @Override
    public String toString() {
        return this.getDay() + "." + this.getMonth() + "." + this.getYear();
    }

    protected boolean dayIsValid(int year, int month, int day) {
        HashSet<Integer> monthsWith30Days = new HashSet<>();
        monthsWith30Days.add(4);
        monthsWith30Days.add(6);
        monthsWith30Days.add(9);
        monthsWith30Days.add(11);

        if (day < 1) {
            return false;
        }
        //Schaltjahr mit 28.Feb
        if (year % 4 == 0 && month == 2 && day > 29) {
            return false;
        }

        if (month == 2 && day > 28) {
            return false;
        }

        if (monthsWith30Days.contains(month) && day > 30) {
            return false;
        }

        if (day > 31) {
            return false;
        }

        return true;
    }

    protected boolean dateIsValid() {
        if (this.year < 0) {
            return false;
        }
        if (this.month < 1 || this.month > 12) {
            return false;
        }

        return dayIsValid(this.year, this.month, this.day);
    }

}
