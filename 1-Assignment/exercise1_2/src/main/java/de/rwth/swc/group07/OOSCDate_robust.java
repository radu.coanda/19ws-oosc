package de.rwth.swc.group07;

import static org.valid4j.Assertive.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InvalidObjectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;

import static java.time.temporal.ChronoUnit.*;

public class OOSCDate_robust implements DateInterface{

    private int year;
    private int month;
    private int day;

    public OOSCDate_robust(int year, int month, int day){
        this.setDate(year, month, day);
    }

    @Override
    public void setDate(int year, int month, int day) throws IllegalArgumentException {
        if(year < 0){
            throw new IllegalArgumentException("year must be greater or equal to zero");
        }

        if(month < 0 || month > 12){
            throw new IllegalArgumentException("month must be between 1 and 12");
        }

        if(!dayIsValid(year, month, day)){
            throw new IllegalArgumentException("day must be a valid day in the given month");
        }

        this.year = year;
        this.month = month;
        this.day = day;
    }

    @Override
    public void setYear(int year) throws IllegalArgumentException {
        if(year < 0){
            throw new IllegalArgumentException("year must be greater or equal to zero");
        }

        this.year = year;
    }

    @Override
    public void setMonth(int month) throws IllegalArgumentException {
        if(month < 1 || month > 12){
            throw new IllegalArgumentException("month must be between 1 and 12");
        }
        this.month = month;
    }

    @Override
    public void setDay(int day) throws IllegalArgumentException {
        if(!dayIsValid(this.year, this.month, day)){
            throw new IllegalArgumentException("day must be a valid day in the given month");
        }
        this.day = day;
    }

    @Override
    public int getYear() throws IllegalArgumentException {
        if(!this.dateIsValid()){
            throw new IllegalStateException("date is not valid");
        }
        return this.year;
    }

    @Override
    public int getMonth() throws IllegalArgumentException {
        if(!this.dateIsValid()){
            throw new IllegalStateException("date is not valid");
        }
        return this.month;
    }

    @Override
    public int getDay() throws IllegalArgumentException {
        if(!this.dateIsValid()){
            throw new IllegalStateException("date is not valid");
        }
        return this.day;
    }

    @Override
    public void addDays(int daysToAdd) throws IllegalArgumentException {
        if(daysToAdd < 0){
            throw new IllegalArgumentException("daysToAdd must be greater or equal to zero");
        }
        LocalDate date = LocalDate.of(this.year, this.month, this.day).plusDays(daysToAdd);
        this.year = date.getYear();
        this.month = date.getMonthValue();
        this.day = date.getDayOfMonth();
    }

    @Override
    public void addMonths(int monthsToAdd) throws IllegalArgumentException {
        if(monthsToAdd < 0){
            throw new IllegalArgumentException("monthsToAdd must be greater or equal to zero");
        }

        LocalDate date = LocalDate.of(this.year, this.month, this.day).plusMonths(monthsToAdd);
        this.year = date.getYear();
        this.month = date.getMonthValue();
    }

    @Override
    public void addYears(int yearsToAdd) throws IllegalArgumentException {
        if(yearsToAdd < 0){
            throw new IllegalArgumentException("yearsToAdd must be greater or equal to zero");
        }
        this.year += yearsToAdd;
    }

    @Override
    public void removeDays(int daysToRemove) throws IllegalArgumentException {
        if(daysToRemove < 0){
            throw new IllegalArgumentException("daysToRemove must be greater or equal to zero");
        }
        OOSCDate_robust dateZero = new OOSCDate_robust(0, 1, 1);
        if(daysToRemove > daysBetween(dateZero)){
            throw new IllegalArgumentException("daysToRemove must be less than total number of days in actual date");
        }

        LocalDate date = LocalDate.of(this.year, this.month, this.day).minusDays(daysToRemove);
        this.year = date.getYear();
        this.month = date.getMonthValue();
        this.day = date.getDayOfMonth();
    }

    @Override
    public void removeMonths(int monthsToRemove) throws IllegalArgumentException {
        if(monthsToRemove < 0){
            throw new IllegalArgumentException("monthsToRemove must be greater or equal to zero");
        }

        if(monthsToRemove >= this.year*12+this.month){
            throw new IllegalArgumentException("monthsToRemove must be less than total number of months in actual date");
        }

        LocalDate date = LocalDate.of(this.year, this.month, this.day).minusMonths(monthsToRemove);
        this.year = date.getYear();
        this.month = date.getMonthValue();
    }

    @Override
    public void removeYears(int yearsToRemove) throws IllegalArgumentException {
        if(yearsToRemove < 0){
            throw new IllegalArgumentException("yearsToRemove must be greater or equal to zero");
        }

        if(yearsToRemove > this.year){
            throw new IllegalArgumentException("yearsToRemove must be less than total number of years in date");
        }
        this.year -= yearsToRemove;
    }

    @Override
    public int daysBetween(DateInterface otherDate) throws IllegalArgumentException {
        if(otherDate == null){
            throw new IllegalArgumentException("otherDate must not be null");
        }
        if(!this.dateIsValid()){
            throw new IllegalStateException("date is not valid");
        }
        return timeBetween(DATETYPE_DAY, otherDate);
    }

    @Override
    public int timeBetween(int type, DateInterface otherDate) throws IllegalArgumentException {
        if(otherDate == null){
            throw new IllegalArgumentException("otherDate must not be null");
        }
        if(!this.dateIsValid()){
            throw new IllegalStateException("date is not valid");
        }
        if(type > 3 || type < 1){
            throw new IllegalArgumentException("type must be 0, 1 or 2");
        }

        LocalDate startDate = LocalDate.of(this.year, this.month, this.day);
        LocalDate endDate = LocalDate.of(otherDate.getYear(), otherDate.getMonth(), otherDate.getDay());
        switch (type){
            case DATETYPE_YEAR:
                return (int) startDate.until(endDate, YEARS);
            case DATETYPE_MONTH:
                return (int) startDate.until(endDate, MONTHS);
            case DATETYPE_DAY:
                return (int) startDate.until(endDate, DAYS);
            default:
                return 0;
        }
    }

    @Override
    public void syncWithUTCTimeserver() throws IllegalStateException {
        try {
            URL url = new URL("https://andthetimeis.com/utc/now");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(5000);

            connection.setRequestMethod("GET");
            connection.setReadTimeout(5000);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer fetchedDate = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                fetchedDate.append(inputLine);
            }
            in.close();

            connection.disconnect();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss +00:00");
            LocalDate date = LocalDate.parse(fetchedDate, formatter);

            this.year = date.getYear();
            this.month = date.getMonthValue();
            this.day = date.getDayOfMonth();

        }catch (Exception e){
            System.out.println("There was an error while synchronising with UTC time server: " + e.getMessage());
        }

        if(!this.dateIsValid()){
            throw new IllegalStateException("date is not valid");
        }
    }

    @Override
    public String toString(){
        return this.getDay() + "." + this.getMonth() + "." + this.getYear();
    }

    protected boolean dayIsValid(int year, int month, int day){
        HashSet<Integer> monthsWith30Days = new HashSet<>();
        monthsWith30Days.add(4);
        monthsWith30Days.add(6);
        monthsWith30Days.add(9);
        monthsWith30Days.add(11);

        if (day < 1){
            return false;
        }
        //Schaltjahr mit 28.Feb
        if (year%4 == 0 && month == 2 && day > 29){
            return false;
        }

        if(month == 2 && day > 28){
            return false;
        }

        if(monthsWith30Days.contains(month) && day > 30){
            return false;
        }

        if (day > 31){
            return false;
        }

        return true;
    }
    protected boolean dateIsValid(){
        if (this.year < 0){
            return false;
        }
        if(this.month < 1 || this.month > 12){
            return false;
        }

        return dayIsValid(this.year, this.month, this.day);
    }

}
