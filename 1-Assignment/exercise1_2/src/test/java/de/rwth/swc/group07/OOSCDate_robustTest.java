package de.rwth.swc.group07;

import org.junit.Assert;
import org.junit.Test;

import java.time.*;

public class OOSCDate_robustTest {

    private final int YEAR = 2019;
    private final int MONTH = 10;
    private final int DAY = 19;

    @Test(expected = IllegalArgumentException.class)
    public void setDate(){
        OOSCDate_robust date = new OOSCDate_robust(0, 1, 1);
        date.setDate(-12, MONTH, DAY);
        date.setDate(YEAR, 13, DAY);
        date.setDate(YEAR, MONTH, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setYear() {
        OOSCDate_robust date = new OOSCDate_robust(YEAR, MONTH, DAY);
        date.setYear(-2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setMonth() {
        OOSCDate_robust date = new OOSCDate_robust(YEAR, MONTH, DAY);
        date.setMonth(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setDay() {
        OOSCDate_robust date = new OOSCDate_robust(YEAR, MONTH, DAY);
        date.setDay(32);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addDays() {
        OOSCDate_robust date = new OOSCDate_robust(YEAR, MONTH, DAY);
        date.addDays(-35);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addMonths() {
        OOSCDate_robust date = new OOSCDate_robust(YEAR, MONTH, DAY);
        date.addMonths(-3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addYears() {
        OOSCDate_robust date = new OOSCDate_robust(YEAR, MONTH, DAY);
        date.addYears(-5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeDays() {
        OOSCDate_robust date = new OOSCDate_robust(YEAR, MONTH, DAY);
        date.removeDays(-5);
        date.removeDays(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeMonths() {
        OOSCDate_robust date = new OOSCDate_robust(YEAR, MONTH, DAY);
        date.removeMonths(-15);
        date.removeMonths(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeYears() {
        OOSCDate_robust date = new OOSCDate_robust(YEAR, MONTH, DAY);
        date.removeYears(-25);
        date.removeYears(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void daysBetween() {
        OOSCDate_robust date1 = new OOSCDate_robust(YEAR, MONTH, DAY);
        OOSCDate_robust date2 = null;
        date1.daysBetween(date2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void timeBetween() {
        OOSCDate_robust date1 = new OOSCDate_robust(YEAR, MONTH, DAY);
        OOSCDate_robust date2 = null;
        date1.timeBetween(0, date2);
        date1.timeBetween(1, date2);
        date1.timeBetween(2, date2);
    }
}
