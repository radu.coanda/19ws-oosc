package de.rwth.swc.group07;

import org.junit.Assert;
import org.junit.Test;

import java.time.*;

public class OOSCDateTest {

    private final int YEAR = 2019;
    private final int MONTH = 10;
    private final int DAY = 19;

    @Test
    public void setDate(){
        OOSCDate date = new OOSCDate(0, 1, 1);
        date.setDate(YEAR, MONTH, DAY);

        Assert.assertEquals(YEAR, date.getYear());
        Assert.assertEquals(MONTH, date.getMonth());
        Assert.assertEquals(DAY, date.getDay());
    }

    @Test
    public void setYear() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.setYear(2000);

        Assert.assertEquals(2000, date.getYear());
    }

    @Test
    public void setMonth() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.setMonth(2);

        Assert.assertEquals(2, date.getMonth());
    }

    @Test
    public void setDay() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.setDay(2);

        Assert.assertEquals(2, date.getDay());
    }

    @Test
    public void getYear() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        Assert.assertEquals(YEAR, date.getYear());
    }

    @Test
    public void getMonth() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        Assert.assertEquals(MONTH, date.getMonth());
    }

    @Test
    public void getDay() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        Assert.assertEquals(DAY, date.getDay());
    }

    @Test
    public void addDays() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.addDays(35);
        Assert.assertEquals(YEAR, date.getYear());
        Assert.assertEquals(11, date.getMonth());
        Assert.assertEquals(23, date.getDay());
    }

    @Test
    public void addMonths() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.addMonths(3);
        Assert.assertEquals(2020, date.getYear());
        Assert.assertEquals(1, date.getMonth());
        Assert.assertEquals(DAY, date.getDay());
    }

    @Test
    public void addYears() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.addYears(5);
        Assert.assertEquals(2024, date.getYear());
        Assert.assertEquals(MONTH, date.getMonth());
        Assert.assertEquals(DAY, date.getDay());
    }

    @Test
    public void removeDays() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.removeDays(19);
        Assert.assertEquals(YEAR, date.getYear());
        Assert.assertEquals(9, date.getMonth());
        Assert.assertEquals(30, date.getDay());
    }

    @Test
    public void removeMonths() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.removeMonths(15);
        Assert.assertEquals(2018, date.getYear());
        Assert.assertEquals(7, date.getMonth());
        Assert.assertEquals(DAY, date.getDay());
    }

    @Test
    public void removeYears() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        date.removeYears(25);
        Assert.assertEquals(1994, date.getYear());
        Assert.assertEquals(MONTH, date.getMonth());
        Assert.assertEquals(DAY, date.getDay());
    }

    @Test
    public void daysBetween() {
        OOSCDate date1 = new OOSCDate(YEAR, MONTH, DAY);
        OOSCDate date2 = new OOSCDate(YEAR+1, MONTH, DAY);
        Assert.assertEquals(366, date1.daysBetween(date2));
    }

    @Test
    public void timeBetween() {
        OOSCDate date1 = new OOSCDate(YEAR, MONTH, DAY);
        OOSCDate date2 = new OOSCDate(YEAR+1, MONTH, DAY);
        Assert.assertEquals(1, date1.timeBetween(0, date2));
        Assert.assertEquals(12, date1.timeBetween(1, date2));
        Assert.assertEquals(366, date1.timeBetween(2, date2));
    }

    @Test
    public void syncWithUTCTimeServer() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);

        ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
        date.syncWithUTCTimeserver();

        Assert.assertEquals(utc.getYear(), date.getYear());
        Assert.assertEquals(utc.getMonthValue(), date.getMonth());
        Assert.assertEquals(utc.getDayOfMonth(), date.getDay());
    }

    @Test
    public void toStringTest() {
        OOSCDate date = new OOSCDate(YEAR, MONTH, DAY);
        Assert.assertEquals("19.10.2019", date.toString());
    }
}
