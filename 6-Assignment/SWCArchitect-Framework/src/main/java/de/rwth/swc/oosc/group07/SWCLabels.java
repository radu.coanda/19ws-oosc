package de.rwth.swc.oosc.group07;

import org.jhotdraw.draw.DrawLabels;
import org.jhotdraw.util.ResourceBundleUtil;

import java.util.ResourceBundle;

public class SWCLabels {
    private SWCLabels() {
        // prevent instance creation
    }
    public static ResourceBundleUtil getLabels() {
        ResourceBundleUtil labels = new ResourceBundleUtil(ResourceBundle.getBundle("de.rwth.swc.oosc.group07.Labels"));
        labels.setBaseClass(SWCLabels.class);
        return labels;
    }
}
