package de.rwth.swc.oosc.swcarchitect.webservice.rest;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.rwth.swc.oosc.swcarchitect.webservice.domain.Image;

/**
 * Created by andy on 14.01.16.
 */

@RestController
@RequestMapping("/images")
public class ImageResource {

    private Logger logger;
    private List<Image> data = new ArrayList<>();

    /**
     * Get all images in the standard representation format defined by the Domain Entities
     **/
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Image> getAllImages() {

        logger.info("Sending {} Images", data.size());
        return data;
    }
    
    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public List<Image> getImagesByName(@RequestParam(value="filter") String filter) {

        List<Image> ImagesWithName = new ArrayList<>();
        
        for (Iterator iterator = data.iterator(); iterator.hasNext();) {
			Image image = (Image) iterator.next();
			if(image.getName().contains(filter)){
				ImagesWithName.add(image);
			}
		}
        return ImagesWithName;
    }
    
    @RequestMapping("/owner")
    public String ownerInfo(@RequestParam(value="name",defaultValue = "Unkown")String name)
    {
    	return "This Collection is owned by " + name;
    }
    
    @RequestMapping(value="/{nr}",params="format=html",produces="text/html")
    public String showImageAsHTML(@PathVariable("nr") int nr)
    {
    	Image image= data.get(nr);
    	
    	return "<html><body><img src=\""+image.getLocation().toString()+"></body></html>";
    }


    @PostConstruct
    public void setup() throws Exception {

        logger = LoggerFactory.getLogger(this.getClass());


        data.add(new Image(1,"Sports", new URL("http://lorempixel.com/400/200/sports/")));
        data.add(new Image(2,"City", new URL("http://lorempixel.com/1280/1024/city/")));
        data.add(new Image(3,"Food", new URL("http://lorempixel.com/500/500/food/")));
        data.add(new Image(4,"Cats", new URL("http://lorempixel.com/640/480/cats/")));
        data.add(new Image(5,"Nature", new URL("http://lorempixel.com/800/600/nature/")));
    }

}
