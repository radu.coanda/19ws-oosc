package de.rwth.swc.oosc.swcarchitect.webservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwcArchitectWebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwcArchitectWebserviceApplication.class, args);
	}
}
